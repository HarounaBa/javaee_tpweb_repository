<%@page import="org.harouna.model.Musician"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Musician list</title>
</head>
<body>

	<h2>Musician Musician List</h2>
	<%
		List<Musician> list = (List<Musician>) request.getAttribute("list-musician");
	%>
	<table border="0" cellpadding="3" cellspacing="3">
		<tr>
			<td>ID</td>
			<td>Name</td>
			<td>Date of Birth</td>
			<td>Music Type</td>
		</tr>
		<%
			for (Musician m : list) {
		%>
		<tr>
			<td><%=m.getId()%></td>
			<td><%=m.getName()%></td>
			<td><%=m.getDateOfBirth()%></td>
			<td><%=m.getMusicType()%></td>
		</tr>
		<% } %>
	</table>
</body>
</html>