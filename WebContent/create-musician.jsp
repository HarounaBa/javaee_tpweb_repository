<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Musician creation</title>
</head>
<body>
	<%
		if (request.getAttribute("id") != null) {
	%>
	<h2>
		Musician
		<%=request.getAttribute("id")%>
		created
	</h2>
	<%
		}
	%>
	<h2>Musician creation</h2>
	<form action="create-musician" method="POST">
		<table border="0" cellpadding="3" cellspacing="3">
			<tr>
				<td>Name:</td>
				<td><input type="text" name="name" size="20" /></td>
			</tr>
			<tr>
				<td>Date of birth:</td>
				<td><input type="text" name="date-of-birth" size="20" /></td>
			</tr>
			<tr>
				<td>Music type:</td>
				<td><input type="text" name="music-type" size="20" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="OK" /></td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</form>
</body>
</html>