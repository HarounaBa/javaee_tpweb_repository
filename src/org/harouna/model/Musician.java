package org.harouna.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@NamedQueries(
		@NamedQuery(
				name="Musicians.getAll", 
				query="select m from Musician m order by m.name"
				)
		)
@Entity(name="Musician")
@Table(name="t_musician")
public class Musician {

	@Column(name="pkid")
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private long id;

	@Column(name="musician_name", length=64)
	private String name;

	@Temporal(TemporalType.DATE)
	@Column(name="date_of_birth")
	private Date dateOfBirth;

	@Enumerated(EnumType.STRING)
	@Column(name="music_type", length=15)
	private MusicType musicType;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public MusicType getMusicType() {
		return musicType;
	}

	public void setMusicType(MusicType musicType) {
		this.musicType = musicType;
	}

	@Override
	public String toString() {
		return "Musician [id=" + id + ", name=" + name + ", dateOfBirth="
				+ dateOfBirth + ", musicType=" + musicType + "]";
	}
}
