package org.harouna.model;

public enum MusicType {
	JAZZ, ROCK, FOLK, CLASSICAL, MBALAKH, SLAM
}
