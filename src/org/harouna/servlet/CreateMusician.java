package org.harouna.servlet;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.harouna.ejb.MusicianEJB;
import org.harouna.model.MusicType;
import org.harouna.model.Musician;

@SuppressWarnings("serial")
@WebServlet(urlPatterns="/create-musician")
public class CreateMusician extends HttpServlet{
	
	@EJB
	private MusicianEJB musicianEJB;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		String name = request.getParameter("name");
		String dateOfBirthParam = request.getParameter("date-of-birth");
		String musicTypeParam = request.getParameter("music-type");
		
		MusicType musicType = MusicType.valueOf(musicTypeParam);
		
		String[] split = dateOfBirthParam.split("/");
		int day = Integer.parseInt(split[0]);
		int month = Integer.parseInt(split[1]);
		int year = Integer.parseInt(split[2]);
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month - 1, day - 1);
		Date dateOfBirth = calendar.getTime();
		
		Musician musician = new Musician();
		musician.setName(name);
		musician.setDateOfBirth(dateOfBirth);
		musician.setMusicType(musicType);
		
		long id = musicianEJB.createMusician(musician);
		
		request.setAttribute("id", id);
		
		RequestDispatcher rd = request.getRequestDispatcher("/create-musician.jsp");
		rd.forward(request, response);
	}
}
