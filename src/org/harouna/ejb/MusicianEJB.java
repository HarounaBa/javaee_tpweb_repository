package org.harouna.ejb;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.harouna.model.Musician;

public class MusicianEJB {

	@PersistenceContext(unitName="jpa-web")
	private EntityManager em;

	public long createMusician(Musician musician) {
		em.persist(musician) ;
		return musician.getId();
	}

	@SuppressWarnings("unchecked")
	public List<Musician> getAllMusicians() {
		Query query = em.createNamedQuery("Musicians.getAll");
		List<Musician> list = query.getResultList();
		return list;
	}
}
